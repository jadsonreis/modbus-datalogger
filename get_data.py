from parser import get_data_of_registers
from parser import get_registers
from pymongo import MongoClient
import xml.etree.ElementTree as ET
import argparse
from datetime import datetime

def get_len(index, block, model_len):
    if (index + 1) == len(block):
        return model_len -  int(block[index].attrib['offset'])
    else:
        return int(block[index + 1].attrib['offset']) -  int(block[index].attrib['offset'])

def load_model(model, initial_regs):
    tree = ET.parse(model)
    root = tree.getroot()
    block = root.find('model/block')
    model_len = int(root.find('model').attrib['len'])

    table_attrs = {}
    for index, point in enumerate(block): 
        attrs = point.attrib
        type_attr = 'int' if 'int' in attrs['type'] else attrs['type']
        type_attr = 'float' if 'float' in attrs['type'] else type_attr

        len_attr = int(attrs['len']) if 'len' in attrs.keys() else get_len(index, block, model_len)

        result = get_data_of_registers(
            len_attr,
            initial_regs + int(attrs['offset']) - 1,
            type_attr)

        table_attrs[attrs['id']] = result

        print("{}: {}".format(attrs['id'], result))

    return table_attrs

def auto_select_model(model):
    if 'Fronius' in model: name = 'Fronius'
    switcher = {
        'TestInverter':  {'get_xml_format': 40070, 'initial_register': 40072},
        'Fronius':  {'get_xml_format': 40070, 'initial_register': 40072},
        'SMA':  {'get_xml_format': 40186, 'initial_register': 40188},
    }
    return switcher[name]

detect_xml_file = lambda n: '/home/pi/MundoDevOps/datalogger-modbus/models/smdx_{0:05d}.xml'.format(n)
#detect_xml_file = lambda n: 'models/smdx_{0:05d}.xml'.format(n)

if __name__ == "__main__":
    print ('Commmon Model')
    defaut_model='/home/pi/MundoDevOps/datalogger-modbus/models/smdx_00001.xml'
    #defaut_model='models/smdx_00001.xml'
    default_register=40005
#
    table_attrs_common_model = load_model(defaut_model, default_register)
    select_new_xml = auto_select_model(table_attrs_common_model['Mn'])
    new_xml = get_registers(1, select_new_xml['get_xml_format'] - 1)

    print ('\n')
    print (detect_xml_file(new_xml[0]))
    inverter_model = detect_xml_file(new_xml[0])
    print ('\n')

    data_list = load_model(inverter_model, select_new_xml['initial_register'])
    data_list.update(table_attrs_common_model)
    data_list['sended'] = False
    data_list['datetime'] = datetime.now()
    # print(data_list)

    client = MongoClient()
    db = client['datalogger']
    db.logs.insert(data_list)

    #print (detect_xml_file(select_new_xml))

    # if table_attrs_common_model['Md'] == 'Fronius':

#    parser = argparse.ArgumentParser()
#    parser.add_argument(
#    "-m", "--model", help="Xml model file",
#    default='models/smdx_00001.xml')
#    parser.add_argument("-i", "--initial-reg",
#            help="ID Code of initial register.",
#            type=int, default=40005)
#
#    args = parser.parse_args()


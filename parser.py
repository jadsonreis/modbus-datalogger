from pyModbusTCP.client import ModbusClient
from pyModbusTCP import utils
import argparse
import binascii


def read_float(number, address):
    c = ModbusClient(host="192.168.1.121", port=502, auto_open=True)
    reg_l = c.read_holding_registers(address, number)
    if reg_l:
        result = [utils.decode_ieee(f) for f in utils.word_list_to_long(reg_l)]
        return result
    else:
        return None

def bit_to_string(int_number):
    try:
        int_number >= 16
        return binascii.unhexlify("%x" % int_number).decode()
    except:
        return ''

def get_registers(num_of_registers, position):
    c = ModbusClient(host="192.168.1.121", port=502, auto_open=True)
    return c.read_holding_registers(position, num_of_registers)

def concat_words(num_of_registers, position):
    result = ''
    registers = get_registers(num_of_registers, position)

    for i in registers:
        result += bit_to_string(i)

    return result

def none_function(*args, **kwargs):
    return None

def get_data_of_registers(num_of_registers, position, argument):
    method = {
        'int': get_registers,
        'sunssf': get_registers,
        'acc32': get_registers,
        'enum16': get_registers,
        'bitfield32': get_registers,
        'float': read_float,
        'pad': none_function,
        'string': concat_words }

    return method[argument](num_of_registers, position)

if "__main__" == __name__:
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--number-regs",
            help="Num of register needed.",
            type=int, default=1)
    parser.add_argument("-i", "--initial-reg",
            help="ID Code of initial register.",
            type=int, default=40001)
    parser.add_argument("-t", "--argument-type",
            help="Type of data register.",
            type=str, default='int')


    args = parser.parse_args()

    result = get_data_of_registers(
        num_of_registers=args.number_regs,
        position=args.initial_reg -1,
        argument = args.argument_type)

    print(result)
